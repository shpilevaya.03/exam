﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core
{
    public static class ExtensionMethods
    {
        public static void PrintArray(this int[] a)
        {
            foreach (int i in a) Console.WriteLine(i);
        }

        public static string ArrToString<T>(this T[] array)
        {
            var str = "";
            foreach (T i in array)
                str += $"{i} ";
            return str;
        }
    }
}
