﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core
{
    public interface IAlgorithm
    {
        DataPath? AlgorithmPath { get; }
        public void Execute();
    }
}
