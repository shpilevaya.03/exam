﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace Project.Core
{
    internal static class GenerationData
    {
        /// <summary>Генерирует список рандомных чисел, длина которых увеличивается арифметически значением step</summary>
        public static BigInteger[][] GenerateLongData(int count, int stepCount, int step)
        {
            BigInteger[][] data = new BigInteger[stepCount][];
            Random random = new Random();
            BigInteger[] seed = new BigInteger[count];

            StringBuilder number = new StringBuilder();

            for (int i = 0; i < stepCount; i++)
            {
                for (int j = 0; j < count; j++)
                {
                    for (int k = 0; k < (i+1)*step; k++)
                    {
                        number.Append(random.Next(1, 10));
                    }

                    seed[j] = BigInteger.Parse(number.ToString());
                    number.Clear();
                }

                data[i] = seed;
                seed = new BigInteger[count];
            }

            return data;
        }

        /// <summary>Генерирует список рандомных чисел, количество которых равно count</summary>
        public static List<int> GenerateData(int count)
        {
            int[] data = new int[count];
            Random rand = new Random();

            for (int i = 0; i < count; i++)
            {
                data[i] = rand.Next();
            }

            return data.ToList();
        }

        static int[,] GetMatrix(int h, int k)
        {
            var random = new Random();
            int[,] matrix = new int[h, k];

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = random.Next(10000);
                }
            }

            return matrix;
        }

        /// <summary>Генерация матриц</summary>
        /// <returns></returns>
        public static List<int[,]> GenerateMatrices(int count)
        {
            List<int[,]> matrices = new List<int[,]>();

            for (int h = 1; h < count + 1; h++)
            {
                for (int k = 1; k < count + 1; k++)
                {
                    matrices. Add(GetMatrix(h, k));
                    matrices.Add(GetMatrix(k, h));
                }
            }
            return matrices;
        }
    }
}