﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class QuickSort : IAlgorithm
    {
        public DataPath? AlgorithmPath => throw new NotImplementedException();

        public void Execute()
        {
            int[] array = new int[] { 23, 4, -88, -312, -333 };
            Console.WriteLine($"Массив: {array.ArrToString()}");
            Console.WriteLine($"Результат сортировки: {Sort(array).ArrToString()}");
        }

        int[] Sort(int[] array)
        {
            var result = (int[])array.Clone();

            Qsort(result, 0, result.Length - 1);

            return result;
        }

        void Qsort(int[] array, int start, int end)
        {
            if (start >= end) return;

            int leftEdge = PartOfQsort(array, start, end);
            Qsort(array, start, leftEdge - 1);
            Qsort(array, leftEdge, end);
        }

        int PartOfQsort(int[] array, int left, int right)
        {
            int pivot = array[(right + left) / 2];

            while (left <= right)
            {
                while (array[left] < pivot) left++; 
                while (array[right] > pivot) right--;

                if(left <= right) {
                    var temp = array[left];
                    array[left] = array[right];
                    array[right] = temp;
                    left++; right--;
                }
            }

            return left;
        }
    }
}
