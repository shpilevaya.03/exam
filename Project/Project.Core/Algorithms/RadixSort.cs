﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class RadixSort : IAlgorithm, ISort
    {
        public DataPath? AlgorithmPath => DataPath.text;

        public void Execute()
        {
            //int[] array = new int[] { 13, 2, 9, 8, 4, 6, 3, 5, 7, 15, 10, 11, 14, 12, 1 };

            //string[] array = new string[] { "Carmen", "Adela", "Beatrix", "Abbey", "Abigale", "Barbara", "Camalia", "Belinda", "Beckie" };

            FileReader fileReader = new FileReader(DataPath.text);

            AlgorithmWorker.RepeatMeasure(fileReader.ReadWords().ToList(), Sort, 1000, 27);

            //Console.WriteLine($"Массив: {array.ArrToString()}");
            //Console.WriteLine($"Результат сортировки: {Sort(array).ArrToString()}");
        }

        public string[] Sort(string[] words)
        {
            var result = (string[])words.Clone();

            LinkedList<string>[] list = new LinkedList<string>[26];
            list = Array.ConvertAll(list, x => x = new LinkedList<string>());

            Recursive(result, 0, result.Length - 1, 0);

            return result;
        }

        void Recursive(string[] words, int start, int end, int depth)
        {
            if (start >= end) return;

            Queue<string>[] list = new Queue<string>[26];
            list = Array.ConvertAll(list, x => x = new Queue<string>());

            for (int i = start; i <= end; i++)
            {
                if (words[i].Length <= depth) { list[0].Enqueue(words[i]); continue; }

                list[char.ToUpper(words[i][depth]) - 65].Enqueue(words[i]);
            }

            int count = start;
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i].Count - 1 == end - start) return;

                if (list[i].Count == 0) continue;

                count = start;

                while (list[i].Count != 0)
                {
                    words[start] = list[i].Dequeue();
                    start++;
                }

                Recursive(words, count, start - 1, depth + 1);
            }
        }

        int[] Sort(int[] array)
        {
            var result = (int[])array.Clone();
            Queue<int>[] list = new Queue<int>[10];
            list = Array.ConvertAll(list, x => x = new Queue<int>());

            int radix = 1;
            while (true)
            {
                for (int i = 0; i < result.Length; i++)
                {
                    list[result[i] / radix % 10].Enqueue(result[i]);
                }

                if (list[0].Count == result.Length) break;

                int count = 0;
                for (int i = 0; i < list.Length; i++)
                {
                    while (list[i].Count != 0)
                    {
                        result[count] = list[i].Dequeue();
                        count++;
                    }
                }

                radix *= 10;
            }

            return result;
        }
    }
}
