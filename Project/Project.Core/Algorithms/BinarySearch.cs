﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class BinarySearch : IAlgorithm
    {
        public DataPath? AlgorithmPath => throw new NotImplementedException();

        public void Execute()
        {
            //Поиск элемента в отсортированногм массиве
            int[] array = new int[] { 1 };
            Console.WriteLine($"Массив: {array.ArrToString()}");
            Console.Write("Введите ключ-значение: ");
            int key = int.Parse(Console.ReadLine());
            Console.WriteLine($"Результат поиска: {FindIndex(array, key)}");
        }

        int FindIndex(int[] array, int key)
        {
            var left = 0;
            var right = array.Length - 1;

            while (left <= right)
            {
                var mid = left + (right - left) / 2;

                if (array[mid] > key) right = mid - 1;
                else if (array[mid] < key) left = mid + 1;
                else return mid;
            }

            return -1;
        }
    }
}
