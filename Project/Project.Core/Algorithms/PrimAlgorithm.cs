﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class PrimAlgorithm : IAlgorithm
    {
        public DataPath? AlgorithmPath => throw new NotImplementedException();

        bool[] vertices { get; set; }

        public void Execute()
        {
            int[,] array = new int[,]
            {
                { 0, 7, 0,  5, 0,  0,  0 },
                { 7, 0, 8,  9, 7,  0,  0 },
                { 0, 8, 0,  0, 5,  0,  0 },
                { 5, 9, 0,  0, 15, 6,  0 },
                { 0, 7, 5, 15, 0,  8,  9 },
                { 0, 0, 0,  6, 8,  0,  11 },
                { 0, 0, 0,  0, 9,  11, 0 },
            };

            vertices = new bool[array.GetLength(0)];
            FindTree(array).ForEach(x => Console.WriteLine(x.Vertex1 + " " + x.Vertex2 + " " + x.Width));
        }

        (int ind1, int ind2, int width) FindMinAdjacentVertex(int[,] array)
        {
            int min = int.MaxValue;
            int ind1 = -1, ind2 = -1;

            for (int i = 0; i < array.GetLength(0); i++)
            {
                if (vertices[i])
                {
                    for (int j = 0; j < array.GetLength(1); j++)
                    {
                        var curr = array[i, j];
                        if (curr != 0 && curr < min && !vertices[j])
                        {
                            ind1 = i;
                            min = curr;
                            ind2 = j;
                        }
                    }
                }
            }

            return (ind1, ind2, min);
        }

        List<(int Vertex1, int Vertex2, int Width)> FindTree(int[,] array)
        {
            vertices[0] = true;
            List<(int, int, int)> result = new List<(int, int, int)>();

            while (result.Count != array.GetLength(0) - 1)
            {
                (int ind1, int ind2, int width) end = FindMinAdjacentVertex(array);

                result.Add((end.ind1, end.ind2, end.width));
                vertices[end.ind2] = true;
            }

            return result;
        }
    }
}
