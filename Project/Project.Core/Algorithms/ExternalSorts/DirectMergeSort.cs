﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms.ExternalSorts
{
    public class DirectMergeSort : IAlgorithm
    {
        public DataPath? AlgorithmPath => DataPath.data;

        string fileA = "a.txt";
        string fileB = "b.txt";
        int _iterations = 1, segments = 0;
        FileWriter _writer;

        public void Execute()
        {
            int[] array = new int[] { 13, 2, 9, 8, 4, 6, 3, 5, 7, 15, 10, 11, 14, 12, 1 };
            Console.WriteLine($"Массив: {array.ArrToString()}");
            Console.WriteLine($"Результат сортировки: {Sort(array).ArrToString()}");
            Dispose();
        }

        int[] Sort(int[] array)
        {
            _writer = new FileWriter();
            _writer.Write(array);

            while(segments != 1)
            {
                Split();
                Merge();
            }

            int[] result = new int[array.Length];
            using (var reader = new StreamReader(_writer.FullPath))
            {
                for (int i = 0; i < result.Length; i++)
                {
                    result[i] = int.Parse(reader.ReadLine());
                }
            }

            return result;
        }

        void Split()
        {
            using (var reader = new StreamReader(_writer.FullPath))
            using (var writerA = new StreamWriter(fileA))
            using (var writerB = new StreamWriter(fileB))
            {
                segments = 0;
                int count = 0;
                bool flag = false;

                while(!reader.EndOfStream)
                {
                    if (count % _iterations == 0) { 
                        flag = !flag;
                        segments++;
                    }

                    if(flag)
                        writerA.WriteLine(reader.ReadLine());
                    else
                        writerB.WriteLine(reader.ReadLine());

                    count++;
                }
            }
        }

        void Merge()
        {
            using (var readerA = new StreamReader(fileA))
            using (var readerB = new StreamReader(fileB))
            using (var writer = new StreamWriter(_writer.FullPath))
            {
                bool pickA = false, pickB = false;
                int countA = _iterations, countB = _iterations;
                int elemA = 0, elemB = 0;
                while(!readerA.EndOfStream || !readerB.EndOfStream || pickA || pickB)
                {
                    if (countA == 0 && countB == 0)
                    {
                        countA = _iterations;
                        countB = _iterations;
                    }

                    if(countA > 0 && !pickA && !readerA.EndOfStream)
                    {
                        elemA = int.Parse(readerA.ReadLine());
                        pickA = true;
                    }

                    if(countB > 0 && !pickB && !readerB.EndOfStream)
                    {
                        elemB = int.Parse(readerB.ReadLine());
                        pickB = true;
                    }

                    if (pickA) {
                        if(pickB) {
                            if (elemA > elemB) {
                                writer.WriteLine(elemB);
                                pickB = false; countB--;
                            }
                            else {
                                writer.WriteLine(elemA);
                                pickA = false; countA--;
                            }
                        }
                        else {
                            writer.WriteLine(elemA);
                            pickA = false; countA--;
                        }
                    }
                    else if(pickB){
                        writer.WriteLine(elemB);
                        pickB = false; countB--;
                    }
                }
            }

            _iterations *= 2;
        }

        void Dispose()
        {
            File.Delete(fileA);
            File.Delete(fileB);
        }
    }
}
