﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms.ExternalSorts
{
    public class NaturalMergeSort : IAlgorithm
    {
        public DataPath? AlgorithmPath => DataPath.data;

        string fileA = "a.txt";
        string fileB = "b.txt";
        int segments = 0;
        FileWriter _writer;

        public void Execute()
        {
            int[] array = new int[] { 13, 2, 9, 8, 4, 6, 3, 5, 7, 15, 10, 11, 14, 12, 1 };
            Console.WriteLine($"Массив: {array.ArrToString()}");
            Console.WriteLine($"Результат сортировки: {Sort(array).ArrToString()}");
            Dispose();
        }

        int[] Sort(int[] array)
        {
            _writer = new FileWriter();
            _writer.Write(array);

            while (segments != 1)
            {
                Split();
                Merge();
            }

            int[] result = new int[array.Length];
            using (var reader = new StreamReader(_writer.FullPath))
            {
                for (int i = 0; i < result.Length; i++)
                {
                    result[i] = int.Parse(reader.ReadLine());
                }
            }

            return result;
        }

        void Split()
        {
            using (var reader = new StreamReader(_writer.FullPath))
            using (var writerA = new StreamWriter(fileA))
            using (var writerB = new StreamWriter(fileB))
            {
                segments = 1;
                bool flag = true;
                int elem1 = int.Parse(reader.ReadLine());
                writerA.WriteLine(elem1);

                while (!reader.EndOfStream)
                {
                    int elem2 = int.Parse(reader.ReadLine());

                    if (elem1 > elem2) {
                        if (flag) writerA.WriteLine('`'); else writerB.WriteLine('`');
                        flag = !flag;
                        segments++;
                    }

                    if (flag)
                        writerA.WriteLine(elem2);
                    else
                        writerB.WriteLine(elem2);

                    elem1 = elem2;
                }
            }
        }

        void Merge()
        {
            using (var readerA = new StreamReader(fileA))
            using (var readerB = new StreamReader(fileB))
            using (var writer = new StreamWriter(_writer.FullPath))
            {
                bool pickA = false, pickB = false, endA = false, endB = false;
                int elemA = 0, elemB = 0;
                while (!readerA.EndOfStream || !readerB.EndOfStream || pickA || pickB)
                {
                    if(endA && endB) { endA = false; endB = false; }
                    else if (readerA.EndOfStream && endB && !pickA) { endB = false; }
                    else if (readerB.EndOfStream && endA && !pickB) { endA = false; }

                    if (!endA && !pickA && !readerA.EndOfStream)
                    {
                        var str = readerA.ReadLine();
                        if(str.Equals("`")) endA = true;
                        else
                        {
                            elemA = int.Parse(str);
                            pickA = true;
                        }
                    }

                    if (!endB && !pickB && !readerB.EndOfStream)
                    {
                        var str = readerB.ReadLine();
                        if (str.Equals("`")) endB = true;
                        else
                        {
                            elemB = int.Parse(str);
                            pickB = true;
                        }
                    }

                    if (pickA)
                    {
                        if (pickB)
                        {
                            if (elemA > elemB)
                            {
                                writer.WriteLine(elemB);
                                pickB = false;
                            }
                            else
                            {
                                writer.WriteLine(elemA);
                                pickA = false;
                            }
                        }
                        else
                        {
                            writer.WriteLine(elemA);
                            pickA = false;
                        }
                    }
                    else if (pickB)
                    {
                        writer.WriteLine(elemB);
                        pickB = false;
                    }
                }
            }
        }

        void Dispose()
        {
            File.Delete(fileA);
            File.Delete(fileB);
        }
    }
}
