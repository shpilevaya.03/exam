﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class QueueCheck : IAlgorithm
    {
        public DataPath? AlgorithmPath => throw new NotImplementedException();

        public void Execute()
        {
            MyQueue<int> myQueue = new MyQueue<int>();

            myQueue.Enqueue(10);
            myQueue.Enqueue(20);
            myQueue.Enqueue(30);
            myQueue.Enqueue(1);

            myQueue.Print();

            myQueue.Insert(34, 1);

            myQueue.Print();

            myQueue.Insert(354, myQueue.Count - 1);

            myQueue.Print();
        }
    }
}
