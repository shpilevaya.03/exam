﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class SelectionSort : IAlgorithm
    {
        public DataPath? AlgorithmPath => throw new NotImplementedException();

        public void Execute()
        {
            int[] array = new int[] { 23, 4, -88, -312, -333 };
            Console.WriteLine($"Массив: {array.ArrToString()}");
            Console.WriteLine($"Результат сортировки: {Sort(array).ArrToString()}");
        }

        int[] Sort(int[] array)
        {
            var result = (int[])array.Clone();

            Console.WriteLine("Обходим каждый элемент массива");
            for (int i = 0; i < result.Length - 1; i++)
            {
                var min = i;
                Console.WriteLine($"Задаём индекс минимального элемента {min}\nОбходим каждый элемент больше этого индекса");

                for (int j = i + 1; j < result.Length; j++)
                {
                    Console.WriteLine($"Сравнение текущего минимального элемента {result[min]} и элемента {result[j]}");
                    if (result[min] > result[j])
                        min = j;
                }

                Console.WriteLine($"Меняем элементы {result[i]} и {result[min]} местами");
                var temp = result[i];
                result[i] = result[min];
                result[min] = temp;
            }

            return result;
        }
    }
}
