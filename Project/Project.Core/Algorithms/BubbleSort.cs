﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class BubbleSort : IAlgorithm
    {
        public DataPath? AlgorithmPath => throw new NotImplementedException();

        public void Execute()
        {
            int[] array = new int[] { -3, 4, 88, 1, 3 };
            Console.WriteLine($"Массив: {array.ArrToString()}");
            Console.WriteLine($"Результат сортировки: {Sort(array).ArrToString()}");
        }

        int[] Sort(int[] array)
        {
            int[] result = (int[])array.Clone();

            int flipCount = 0;
            for (int i = 0; i < result.Length - 1; i++)
            {
                Console.WriteLine($"Сортировка по {i}-ой итерации");
                for (int j = 0; j < result.Length - i - 1; j++)
                {
                    Console.WriteLine($"Сравниваются {result[j]} и {result[j+1]} элементы");
                    if (result[j] > result[j + 1]) {
                        Console.WriteLine($"Элементы меняются местами");
                        var temp = result[j];
                        result[j] = result[j + 1];
                        result[j + 1] = temp;
                        flipCount++;
                    }
                }

                Console.WriteLine($"Количество флипов: {flipCount}");
                if (flipCount == 0)
                    break;

                Console.WriteLine($"\nМассив: {result.ArrToString()}\n");
                flipCount = 0;
            }

            Console.WriteLine("Массив отсортирован");
            return result;
        }

        int[] SortA(int[] array)
        {
            int[] result = (int[])array.Clone();

            var flipCount = 0;
            for (int i = 0; i < result.Length - 1; i++)
            {
                for (int j = 0; j < result.Length - i - 1; j++)
                {
                    if (result[j] > result[j + 1])
                    {
                        var temp = result[j];
                        result[j] = result[j + 1];
                        result[j + 1] = temp;
                        flipCount++;
                    }
                }

                if (flipCount == 0)
                    break;
                flipCount = 0;
            }

            return result;
        }
    }
}
