﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class CocktailSort: IAlgorithm
    {
        public DataPath? AlgorithmPath => throw new NotImplementedException();
        public void Execute()
        {
            int[] array = new int[] { 23, 4, -88, -312, -333 };
            Console.WriteLine($"Массив: {array.ArrToString()}");
            Console.WriteLine($"Результат сортировки: {Sort(array).ArrToString()}");
        }

        public int[] SortA(int[] arr)
        {
            int left = 0;
            int right = arr.Length - 1;
            while (left < right)
            {
                for (int i = left; i < right; i++)
                {
                    if (arr[i] > arr[i + 1])
                    {
                        int temp = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = temp;
                    }
                }
                right--;
                for (int i = right; i > left ; i--)
                {
                    if (arr[i - 1] > arr[i])
                    {
                        int temp = arr[i - 1];
                        arr[i - 1] = arr[i];
                        arr[i] = temp;
                    }
                }
                left++;
            }
            return arr;
        }

        int[] Sort(int[] array)
        {
            var result = (int[])array.Clone();

            int leftSorted = 0, rigthSorted = result.Length - 1, counter = 1;
            while (counter != 0)
            {
                counter = 0;

                for (int i = leftSorted; i < rigthSorted; i++)
                {
                    if (result[i + 1] < result[i]) {
                        var temp = result[i];
                        result[i] = result[i + 1];
                        result[i + 1] = temp;
                        counter++;
                    }
                }
                rigthSorted--;

                if (counter == 0) break;

                for (int i = rigthSorted; i >= leftSorted; i--)
                {
                    if (result[i + 1] < result[i]) {
                        var temp = result[i];
                        result[i] = result[i + 1];
                        result[i + 1] = temp;
                        counter++;
                    }
                }
                leftSorted++;

                if (counter == 0) break;
            }

            return result;
        }
    }
}
