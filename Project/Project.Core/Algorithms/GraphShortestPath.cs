﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class GraphShortestPath : IAlgorithm
    {
        public DataPath? AlgorithmPath => throw new NotImplementedException();

        int[] weights { get; set; }
        bool[] vertices { get; set; }

        public void Execute()
        {
            int[,] array = new int[,]
            {
                //a  b  c  d  e  f  g
                { 0, 7,  9,  0,  0, 14 },
                { 7, 0,  10, 15, 0, 0 },
                { 9, 10, 0,  11, 0, 2 },
                { 0, 15, 11, 0,  6, 0 },
                { 0,  0, 0,  6,  0, 9 },
                { 14, 0, 2,  0,  9, 0 },
            };

            weights = new int[array.GetLength(0)];
            vertices = new bool[array.GetLength(0)];

            Console.WriteLine(ShortestPath(array, 0, 4));
        }

        void Recursive(int[,] array, int curr, int end)
        {
            if (curr == end) return;

            int min = int.MaxValue;
            int ind = 0;
            for (int i = 0; i < array.GetLength(0); i++)
            {
                if (array[curr, i] == 0 || vertices[i]) continue;

                weights[i] = weights[i] < weights[curr] + array[curr, i] ? weights[i] : weights[curr] + array[curr, i];

                if (min > weights[i])
                {
                    min = weights[i];
                    ind = i;
                }
            }

            vertices[curr] = true;
            Recursive(array, ind, end);
        }

        int ShortestPath(int[,] array, int first, int second)
        {
            weights = Array.ConvertAll(weights, x => x = int.MaxValue);
            weights[first] = 0;
            int curr = first;


            Recursive(array, curr, second);
            return weights[second];
        }
    }
}
