﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class ShellSort : IAlgorithm
    {
        public DataPath? AlgorithmPath => throw new NotImplementedException();

        public void Execute()
        {
            int[] array = new int[] { 13, 2, 9, 8, 4, 6, 3, 5, 7, 15, 10, 11, 14, 12, 1 };
            Console.WriteLine($"Массив: {array.ArrToString()}");
            Console.WriteLine($"Результат сортировки: {Sort(array).ArrToString()}");
        }

        int[] Sort(int[] array)
        {
            var result = (int[])array.Clone();

            int size = result.Length;

            for (int s = size / 2; s > 0; s /= 2)
            {
                for (int i = s; i < result.Length; i++)
                {
                    for (int j = i - s; j >= 0 && result[j + s] < result[j]; j -= s)
                    {
                        var temp = result[j + s];
                        result[j + s] = result[j];
                        result[j] = temp;
                    }
                }
            }

            return result;
        }
    }
}
