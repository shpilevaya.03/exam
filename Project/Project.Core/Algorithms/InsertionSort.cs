﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class InsertionSort : IAlgorithm
    {

        public DataPath? AlgorithmPath => throw new NotImplementedException();

        public void Execute()
        {
            int[] array = new int[] { 23, 4, -88, -312, -333 };
            Console.WriteLine($"Массив: {array.ArrToString()}");
            Console.WriteLine($"Результат сортировки: {Sort(array).ArrToString()}");
        }

        int[] Sort(int[] array)
        {
            int[] result = (int[])array.Clone();

            Console.WriteLine($"Начинаем проход по элементам массива с 1 (0-ой элемент считается отсортированным)");
            for (int i = 1; i < result.Length; i++)
            {
                var key = result[i];
                var j = i;
                Console.WriteLine($"Ключ-значение: {key}\nЕго индекс: {i}");

                Console.WriteLine($"Начинаем проход назад в отсортированную часть массива, начиная с инд {i}");
                while (j > 0 && key < result[j - 1]) {
                    result[j] = result[j - 1];
                    j--;
                }

                Console.WriteLine($"Вставляем элемент {key} по индексу {j}");
                result[j] = key;
            }

            return result;
        }
    }
}
