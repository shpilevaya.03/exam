﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class BreadthFirstSearch : IAlgorithm
    {
        public DataPath? AlgorithmPath => throw new NotImplementedException();

        Queue<int> queue = new Queue<int>();
        bool[] vertices { get; set; }

        public void Execute()
        {
            int[,] array = new int[,]
            {
                { 0, 1, 1, 1, 0, 0, 0, 0, 0, 0 },
                { 1, 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                { 1, 0, 0, 0, 0, 1, 1, 0, 0, 0 },
                { 1, 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                { 0, 1, 0, 0, 0, 0, 0, 0, 1, 0 },
                { 0, 0, 1, 0, 0, 0, 0, 0, 0, 1 },
                { 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 },
            };

            /*{
                //a  b  c  d  e  f  g
                { 0, 1, 1, 1, 1, 0, 0 },
                { 1, 0, 0, 1, 1, 0, 0 },
                { 1, 0, 0, 0, 0, 1, 1 },
                { 1, 1, 0, 0, 1, 0, 0 },
                { 1, 1, 0, 1, 0, 0, 0 },
                { 0, 0, 1, 0, 0, 0, 1 },
                { 0, 0, 1, 0, 0, 1, 0 }
            };
            /*

            /* 
            int[,] array = new int[,]
            {
                //a  b  c  d  e  f
                { 0, 2, 4, 0, 0, 0 },
                { 2, 0, 1, 0, 7, 0 },
                { 4, 1, 0, 3, 4, 0 },
                { 0, 0, 3, 0, 3, 0 },
                { 0, 7, 4, 3, 0, 2 },
                { 0, 0, 0, 0, 2, 0 },
            };
             */

            vertices = new bool[array.GetLength(0)];

            Traver(array);
        }

        void Recursive(int[,] array)
        {
            if (queue.Count == 0) return;

            int vertex = queue.Peek();
            for (int i = vertex + 1; i < array.GetLength(0); i++)
            {
                if (array[vertex, i] == 0 || vertices[i]) continue;
                    
                Console.WriteLine($"В очередь добавляется вершина {i + 1}\nПуть: {vertex + 1} - {i + 1}");
                queue.Enqueue(i);
                vertices[i] = true;
            }

            queue.Dequeue();

            Console.WriteLine($"Вершина {vertex + 1} пройдена");
            Recursive(array);
        }

        void Traver(int[,] array)
        {
            Console.WriteLine("Помещаем первую вершину в очередь");
            queue.Enqueue(0);
            vertices[0] = true;

            Recursive(array);
        }
    }
}
