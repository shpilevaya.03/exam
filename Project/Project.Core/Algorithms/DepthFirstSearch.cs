﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class DepthFirstSearch : IAlgorithm
    {
        public DataPath? AlgorithmPath => throw new NotImplementedException();

        bool[] vertices { get; set; }

        public void Execute()
        {
            int[,] array = new int[,]
            {
                //a  b  c  d  e  f  g
                { 0, 1, 1, 1, 1, 0, 0 },
                { 1, 0, 0, 1, 1, 0, 0 },
                { 1, 0, 0, 0, 0, 1, 1 },
                { 1, 1, 0, 0, 1, 0, 0 },
                { 1, 1, 0, 1, 0, 0, 0 },
                { 0, 0, 1, 0, 0, 0, 1 },
                { 0, 0, 1, 0, 0, 1, 0 }
            };

            vertices = new bool[array.GetLength(0)];

            Traver(array);
        }

        void Recursive(int[,] array, int vertex)
        {
            if (vertices.Sum(x => x ? 1 : 0) == array.GetLength(0)) return;

            for (int i = vertex + 1; i < array.GetLength(0); i++)
            {
                if (array[vertex, i] != 0 && !vertices[i])
                {
                    Console.WriteLine($"Вершина {i + 1} посещена\nПуть: {vertex + 1} - {i + 1}");
                    vertices[i] = true;
                    Recursive(array, i);
                }
            }
        }

        void Traver(int[,] array)
        {
            Console.WriteLine("Помечаем первую вершину как посещенную");
            vertices[0] = true;

            Console.WriteLine("Начинаем рекурсивный обход по графу, начиная с первой вершины");
            Recursive(array, 0);
        }
    }
}
