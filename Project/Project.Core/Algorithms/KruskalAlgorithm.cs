﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class KruskalAlgorithm : IAlgorithm
    {
        public DataPath? AlgorithmPath => throw new NotImplementedException();

        List<List<int>> trees = new List<List<int>>();

        public void Execute()
        {
            int[,] array = new int[,]
            {
                { 0, 7, 0,  5, 0,  0,  0 },
                { 7, 0, 8,  9, 7,  0,  0 },
                { 0, 8, 0,  0, 5,  0,  0 },
                { 5, 9, 0,  0, 15, 6,  0 },
                { 0, 7, 5, 15, 0,  8,  9 },
                { 0, 0, 0,  6, 8,  0,  11 },
                { 0, 0, 0,  0, 9,  11, 0 },
            };

            //{
            //    //a  b  c  d  e  f
            //    { 0, 2, 4, 0, 0, 0 },
            //    { 2, 0, 1, 0, 7, 0 },
            //    { 4, 1, 0, 3, 4, 0 },
            //    { 0, 0, 3, 0, 3, 0 },
            //    { 0, 7, 4, 3, 0, 2 },
            //    { 0, 0, 0, 0, 2, 0 },
            //};

            FindTree(array).ForEach(x => Console.WriteLine(x.Item1 + " " + x.Item2 + " " + x.Item3));
        }

        List<(int, int, int)> FindEdges(int[,] array)
        {
            List<(int, int, int)> edges = new List<(int, int, int)>();

            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = i + 1; j < array.GetLength(1); j++)
                {
                    if (array[i, j] != 0)
                        edges.Add((i, j, array[i, j]));
                }
            }

            edges.Sort((t1, t2) => t1.Item3.CompareTo(t2.Item3));

            return edges;
        }

        List<(int, int, int)> FindTree(int[,] array)
        {
            var pairs = FindEdges(array);
            var result = new List<(int, int, int)>();

            for (int i = 0; i < pairs.Count; i++)
            {
                if (result.Count == array.GetLength(0) - 1) break;

                bool flag = true;
                int num1 = -1, num2 = -1;
                for (int j = 0; j < trees.Count; j++)
                {
                    bool flag1 = false, flag2 = false;

                    for (int k = 0; k < trees[j].Count; k++)
                    {
                        if (pairs[i].Item1 == trees[j][k])
                            flag1 = true;
                        if (pairs[i].Item2 == trees[j][k])
                            flag2 = true;
                    }

                    if (flag1 && flag2)
                    {
                        flag = false;
                        break;
                    }
                    else if (flag1)
                        num1 = j;
                    else if (flag2)
                        num2 = j;
                }

                if (flag)
                {
                    if (num1 != -1 && num2 != -1)
                    {
                        trees[num1] = trees[num1].Concat(trees[num2]).ToList();
                        trees.Remove(trees[num2]);
                    }
                    else if (num1 != -1)
                        trees[num1].Add(pairs[i].Item2);
                    else if (num2 != -1)
                        trees[num2].Add(pairs[i].Item1);
                    else
                        trees.Add(new List<int>() { pairs[i].Item1, pairs[i].Item2 });

                    result.Add(pairs[i]);
                }
            }

            return result;
        }
    }
}
