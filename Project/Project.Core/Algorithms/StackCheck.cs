﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Core.Algorithms
{
    public class StackCheck : IAlgorithm
    {
        public DataPath? AlgorithmPath => throw new NotImplementedException();

        public void Execute()
        {
            MyStack<int> stack = new MyStack<int>();

            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Push(4);

            stack.Print();

            stack.Pop();

            stack.Print();

            stack.Insert(12, 1);
            stack.Insert(23, 2);

            stack.Print();
        }
    }
}
