﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Core.Algorithms;
using Project.Core.Algorithms.ExternalSorts;

namespace Project.ConsoleUI
{
    class MainMenu : BaseMenu
    {
        protected override List<MenuItem> Items => new List<MenuItem>
        {
            new MenuItem() { Text = "Bubble Sort",      Algorithm = new BubbleSort(), IsSelected = true },
            new MenuItem() { Text = "Insertion Sort",   Algorithm = new InsertionSort()},
            new MenuItem() { Text = "Selection Sort",   Algorithm = new SelectionSort()},
            new MenuItem() { Text = "Shaker Sort",      Algorithm = new CocktailSort()},
            new MenuItem() { Text = "Quick Sort",       Algorithm = new QuickSort()},
            new MenuItem() { Text = "Shell Sort",       Algorithm = new ShellSort()},
            new MenuItem() { Text = "Binary Search",    Algorithm = new BinarySearch()},
            new MenuItem() { Text = "Binary Tree Sort", Algorithm = new BinaryTreeSort()},
            new MenuItem() { Text = "Radix Sort",       Algorithm = new RadixSort()},
            new MenuItem() { Text = "Direct MS",        Algorithm = new DirectMergeSort()},
            new MenuItem() { Text = "Natural MS",       Algorithm = new NaturalMergeSort()},
            new MenuItem() { Text = "ABC Sort",         Algorithm = new ABCSort()},
            new MenuItem() { Text = "Queue",            Algorithm = new QueueCheck()},
            new MenuItem() { Text = "Stack",            Algorithm = new StackCheck()},
            new MenuItem() { Text = "DepthSearch",      Algorithm = new DepthFirstSearch()},
            new MenuItem() { Text = "BreadthSearch",    Algorithm = new BreadthFirstSearch()},
            new MenuItem() { Text = "GraphShortestPath",Algorithm = new GraphShortestPath()},
            new MenuItem() { Text = "KruskalAlgorithm", Algorithm = new KruskalAlgorithm()},
            new MenuItem() { Text = "PrimAlgorithm",    Algorithm = new PrimAlgorithm()},
            new MenuItem() { Text = "HashChainMethod",  Algorithm = new HashTableChainMethod()},
            new MenuItem() { Text = "HashOpenAddress",  Algorithm = new HashTableOpenAddressing()},

            new MenuItem() { Text = "Выход", Algorithm = null }
        };
    }
}
