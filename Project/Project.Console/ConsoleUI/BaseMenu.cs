﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Project.Core;

namespace Project.ConsoleUI
{
    public abstract class BaseMenu
    {
        protected class MenuItem
        {
            public string       Text;
            public bool         IsSelected;
            public IAlgorithm   Algorithm;
        }

        protected abstract List<MenuItem> Items { get; }
        List<MenuItem> _items = new List<MenuItem>();

        public BaseMenu()
        {
            _items = Items.ToList();
        }

        public virtual void Draw()
        {
            ConsoleHelper.ClearScreen();

            foreach (var menuItem in _items)
            {
                Console.BackgroundColor = menuItem.IsSelected
                    ? Color.HighlightColor
                    : Color.BaseColor;

                Console.WriteLine("| " + menuItem.Text);
            }

            Console.BackgroundColor = Color.BaseColor;
        }

        public void Next()
        {
            var selectedItem = _items.First(x => x.IsSelected);
            var selectedInd = _items.IndexOf(selectedItem);
            selectedItem.IsSelected = false;

            selectedInd = selectedInd == _items.Count - 1
                ? 0
                : ++selectedInd;

            _items[selectedInd].IsSelected = true;
        }

        public void Prev()
        {
            var selectedItem = _items.First(x => x.IsSelected);

            var selectedInd = _items.IndexOf(selectedItem);

            selectedItem.IsSelected = false;

            selectedInd = selectedInd <= 0
                ? _items.Count - 1
                : --selectedInd;

            _items[selectedInd].IsSelected = true;
        }

        public bool Select()
        {
            var selectedItem = _items.First(x => x.IsSelected);

            if (selectedItem.Algorithm != null)
            {
                ConsoleHelper.ClearScreen();

                selectedItem.Algorithm.Execute();

                Console.WriteLine();
                ConsoleHelper.PrintHighlightedText("Для выхода нажмите любую клавишу");
                Console.ReadKey();
            }

            return selectedItem.Algorithm == null;
        }
    }
}
